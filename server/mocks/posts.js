/* eslint-env node */

let data = [];

for(let i = 1; i <= 5; i++) {
  data.push({
    id: i,
    type: 'post',
    attributes: {
      title: 'Title '+i,
      content: 'Lorem ipsum'
    },
    relationships: {
      comments: {
        data: [
          { type: "comment", id: i*10 },
          { type: "comment", id: i*10+1 },
          { type: "comment", id: i*10+2 }
        ]
      }
    }
  })
}

module.exports = function(app) {
  let express = require('express');
  let postsRouter = express.Router();



  postsRouter.get('/', function(req, res) {
    res.send({
      'data': data
    });
  });

  postsRouter.post('/', function(req, res) {
    res.status(201).end();
  });

  postsRouter.get('/:id', function(req, res) {
    let item = data.find(x => { return x.id.toString() === req.params.id });
    if (item) {
      res.send({data: item});
    } else {
      res.status(404).end();
    }
  });

  postsRouter.put('/:id', function(req, res) {
    res.send({
      'posts': {
        id: req.params.id
      }
    });
  });

  postsRouter.delete('/:id', function(req, res) {
    res.status(204).end();
  });

  // The POST and PUT call will not contain a request body
  // because the body-parser is not included by default.
  // To use req.body, run:

  //    npm install --save-dev body-parser

  // After installing, you need to `use` the body-parser for
  // this mock uncommenting the following line:
  //
  //app.use('/api/posts', require('body-parser').json());
  app.use('/api/posts', postsRouter);
};
