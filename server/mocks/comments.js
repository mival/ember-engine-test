/* eslint-env node */

let data = [];

for(let i = 1; i <= 5; i++) {
  for(let j = 0; j <= 4; j++) {
    data.push({
      id: i*10+j,
      type: 'comment',
      attributes: {
        'user-name': 'User ' + j,
        content: 'Lorem ipsum comment'
      }
    })
  }
}


module.exports = function(app) {
  let express = require('express');
  let commentsRouter = express.Router();

  commentsRouter.get('/', function(req, res) {
    res.send({
      'data': data
    });
  });

  commentsRouter.post('/', function(req, res) {
    res.status(201).end();
  });

  commentsRouter.get('/:id', function(req, res) {
    let item = data.find(x => { return x.id.toString() === req.params.id });
    if (item) {
      res.send({data: item});
    } else {
      res.status(404).end();
    }
  });

  commentsRouter.put('/:id', function(req, res) {
    res.send({
      'comments': {
        id: req.params.id
      }
    });
  });

  commentsRouter.delete('/:id', function(req, res) {
    res.status(204).end();
  });

  // The POST and PUT call will not contain a request body
  // because the body-parser is not included by default.
  // To use req.body, run:

  //    npm install --save-dev body-parser

  // After installing, you need to `use` the body-parser for
  // this mock uncommenting the following line:
  //
  //app.use('/api/comments', require('body-parser').json());
  app.use('/api/comments', commentsRouter);
};
