import Ember from 'ember';

export default Ember.Component.extend({
  isShow: false,
  actions: {
    toggle() {
      this.toggleProperty('isShow')
    }
  }
});
