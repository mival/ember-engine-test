/*jshint node:true*/
var EngineAddon = require('ember-engines/lib/engine-addon');
module.exports = EngineAddon.extend({
  name: 'chat',
  lazyLoading: false, //Routeless engine

  isDevelopingAddon: function() {
    return true;
  }
});
