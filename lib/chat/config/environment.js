/*jshint node:true*/
'use strict';

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'chat',
    environment: environment
  };

  return ENV;
};
