import buildRoutes from 'ember-engines/routes';

export default buildRoutes(function() {
  this.route('posts', {path: '/'});
  this.route('post', { path: 'post/:id' }, function() {
    this.route('comments', function() {
      this.route('comment', { path: ':id' });
    });
  });
});
